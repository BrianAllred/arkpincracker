﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace ArkPinCracker
{
    public class Program
    {
        /// <summary>
        ///     State tracker
        /// </summary>
        public static State CurrentState;

        public static void Main(string[] args)
        {
            uint startingPin = GetStartingPin();
            uint endingPin = GetEndingPin();
            bool common = GetCommon();
            ClearLog();
            IntPtr windowHandle = GetWindowHandle();

            Console.WriteLine();
            Console.WriteLine("Press Enter to start. Press Esc to stop. Press Ctrl+Shift+P to pause. Press Ctrl+Shift+R to rewind one PIN (press repeatedly to rewind more than once).");
            Console.WriteLine();
            Console.WriteLine("NOTE that rewinding will pause the app. Unpause (Ctrl+Shift+P) to resume.");
            Console.ReadLine();

            // Register hotkeys
            HotKeyManager.RegisterHotKey(Keys.Escape, 0);
            HotKeyManager.RegisterHotKey(Keys.P, KeyModifiers.Shift | KeyModifiers.Control);
            HotKeyManager.RegisterHotKey(Keys.R, KeyModifiers.Shift | KeyModifiers.Control);
            HotKeyManager.HotKeyPressed += HotKeyManagerOnHotKeyPressed;

            // Bring Ark forward
            SetForegroundWindow(windowHandle);

            // Create state
            CurrentState = new State(startingPin, endingPin, common, windowHandle);
            CurrentState.GeneratePins();

            // Start the thread and wait for it to quit
            ThreadPool.QueueUserWorkItem(PinThread, CurrentState);

            while (!CurrentState.Stop)
            {
                Thread.Sleep(100);
            }

            Console.WriteLine("Done!");
            Console.WriteLine("Press enter to close...");
            Console.ReadLine();
        }

        #region User Entry

        private static void ClearLog()
        {
            Console.Write("Would you like to clear old attempts out of the log? [Y/n]: ");
            string clearStr = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(clearStr) || string.Equals(clearStr, "y", StringComparison.CurrentCultureIgnoreCase))
            {
                File.WriteAllText("attempts.log", string.Empty);
            }
        }

        private static bool GetCommon()
        {
            Console.Write("Would you like to try common PINs first? [Y/n]: ");
            string commonStr = Console.ReadLine();
            return string.IsNullOrWhiteSpace(commonStr) || string.Equals(commonStr, "y", StringComparison.CurrentCultureIgnoreCase);
        }

        private static uint GetEndingPin()
        {
            uint pin;
            while (true)
            {
                Console.Write("Enter ending value: ");
                string endStr = Console.ReadLine();
                if (!uint.TryParse(endStr, out pin) || pin > 9999)
                {
                    Console.WriteLine("Error parsing PIN! Please try again.\n");
                    continue;
                }

                break;
            }

            return pin;
        }
        
        private static uint GetStartingPin()
        {
            uint pin;
            while (true)
            {
                Console.Write("Enter starting value: ");
                string startStr = Console.ReadLine();
                if (!uint.TryParse(startStr, out pin))
                {
                    Console.WriteLine("Error parsing PIN! Please try again.\n");
                    continue;
                }

                break;
            }

            return pin;
        }

        private static IntPtr GetWindowHandle()
        {
            Process mainProcess;
            while ((mainProcess = Process.GetProcessesByName("ShooterGame").FirstOrDefault()) == null)
            {
                Console.WriteLine("Ark process not found!");
                Console.WriteLine("Open game and position yourself in front of lock before continuing.");
                Console.WriteLine("Press Enter to continue...");
                Console.ReadLine();

            }

            return mainProcess.MainWindowHandle;
        }

        #endregion

        /// <summary>
        ///     Open the PIN entry dialog,
        ///     wait for it to appear,
        ///     attempt the given PIN,
        ///     log the pin attempted,
        ///     wait before trying again
        /// </summary>
        /// <param name="pin">
        ///     PIN to attempt
        /// </param>
        private static void AttemptCode(string pin)
        {
            SendKeys.SendWait("e");
            Thread.Sleep(650);
            SendKeys.SendWait(pin);
            string log = $"{DateTime.Now} - {pin}";
            Console.WriteLine(log);
            File.AppendAllLines("attempts.log", new[] {log});
            Thread.Sleep(600);
        }

        /// <summary>
        ///     Thread that controls the PIN attempts
        /// </summary>
        /// <param name="state">
        ///     State of the crack
        /// </param>
        private static void PinThread(object state)
        {
            var curState = (State) state;

            while (!curState.Stop)
            {
                if (!curState.Pause)
                {
                    var pin = curState.GetNextPin();
                    if (string.IsNullOrWhiteSpace(pin))
                    {
                        break;
                    }

                    AttemptCode(pin);
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }

            // Set stop state
            curState.Stop = true;
        }

        /// <summary>
        ///     Fired when a hotkey is pressed
        /// </summary>
        /// <param name="sender">
        ///     The hotkey manager
        /// </param>
        /// <param name="e">
        ///     The hotkey event args
        /// </param>
        private static void HotKeyManagerOnHotKeyPressed(object sender, HotKeyEventArgs e)
        {
            // Escape stops the program
            if (e.Key == Keys.Escape)
            {
                CurrentState.Stop = true;
                return;
            }

            // Ctrl+Shift+P pauses the program
            if (e.Key == Keys.P)
            {
                SetForegroundWindow(CurrentState.Handle);
                CurrentState.Pause = !CurrentState.Pause;
                return;
            }

            // Ctrl+Shift+R rewinds the current PIN
            if (e.Key == Keys.R)
            {
                CurrentState.Pause = true;
                CurrentState.Rewind();
            }
        }

        #region DLL Imports
        
        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        #endregion
    }
}
