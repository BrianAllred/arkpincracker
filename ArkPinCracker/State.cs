﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ArkPinCracker
{
    /// <summary>
    ///     Cross-thread state tracker
    /// </summary>
    public class State
    {
        private bool _pause;
        private bool _stop;
        private readonly uint _endingPin;
        private readonly uint _startingPin;
        private readonly bool _common;
        private readonly IntPtr _handle;

        private readonly List<string> _commonPins = new List<string>(new []{
            "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999", "1010", "2020", "3030",
            "4040", "5050", "6060", "7070", "8080", "9090", "4321", "1000", "2000", "3000", "4000", "5000", "6000",
            "7000", "8000", "9000", "6969"
        });

        private readonly LinkedList<string> _pins = new LinkedList<string>();

        private readonly Stack<string> _usedPins = new Stack<string>();

        /// <summary>
        ///     The thread sync object
        /// </summary>
        public readonly object LockObject = new object();

        public State(uint startingPin, uint endingPin, bool common, IntPtr handle)
        {
            _startingPin = startingPin;
            _endingPin = endingPin;
            _common = common;
            _handle = handle;
        }

        /// <summary>
        ///     Whether cracker is paused
        /// </summary>
        public bool Pause
        {
            get
            {
                lock (LockObject)
                {
                    return _pause;
                }
            }
            set
            {
                lock (LockObject)
                {
                    _pause = value;
                }
            }
        }

        /// <summary>
        ///     Whether cracker is stopped
        /// </summary>
        public bool Stop
        {
            get
            {
                lock (LockObject)
                {
                    return _stop;
                }
            }
            set
            {
                lock (LockObject)
                {
                    _stop = value;
                }
            }
        }

        /// <summary>
        ///     The main window handle of the Ark process
        /// </summary>
        public IntPtr Handle
        {
            get
            {
                lock (LockObject)
                {
                    return _handle;
                }
            }
        }

        /// <summary>
        ///     Rewind the queue by popping from the used Pins and inserting to the front of the pins list
        /// </summary>
        public void Rewind()
        {
            lock (LockObject)
            {
                if (_usedPins.Count > 0)
                {
                    _pins.AddFirst(_usedPins.Pop());
                }
            }
        }

        /// <summary>
        ///     Get the next pin in the queue
        /// </summary>
        /// <returns>
        ///     String representing next pin
        /// </returns>
        public string GetNextPin()
        {
            lock (LockObject)
            {
                if (_pins.Count == 0)
                {
                    return null;
                }

                _usedPins.Push(_pins.First.Value);
                _pins.RemoveFirst();
                return _usedPins.Peek();
            }
        }

        /// <summary>
        ///     Generate all of the pins to be tried
        /// </summary>
        public void GeneratePins()
        {
            lock (LockObject)
            {
                if (_common)
                {
                    foreach (var commonPin in _commonPins)
                    {
                        _pins.AddLast(commonPin);
                    }
                }

                for (var i = _startingPin; i <= _endingPin; i++)
                {
                    string pinStr = i.ToString("D4");
                    if (!_pins.Contains(pinStr))
                    {
                        _pins.AddLast(pinStr);
                    }
                }
            }
        }
    }
}
