# ArkPinCracker

[![Build status](https://ci.appveyor.com/api/projects/status/qqwrwu8xa9k8gt47?svg=true)](https://ci.appveyor.com/project/BrianAllred/arkpincracker)

Shamelessly copied idea from [here](https://github.com/m3talstorm/ArkPincodeCracker). Couldn't get that to work, so I built this.

## Usage

* Position your Ark character in front of the locked object.
* Start this application.
* Enter starting and ending PINs.
* Decide if you want the app to attempt 20 or so commonly used PINs first.
* Decide if you want to clear the log out.
* Start the cracking.
* Press `Esc` to stop at anytime.
* Press `Ctrl+Shit+P` to pause. Press the same keys to unpause.
* Press `Ctrl+Shift+R` to rewind. This will automatically pause, so press `Ctrl+Shift+P` to unpause when ready.

## Tips

Multiple players can use this at the same time to cut the search time down. Each PIN attempt takes about 1 second due to waiting for the game and server to respond to inputs. On average, it will take one person 2 hours to crack a PIN, while 4 players can do it in roughly 20 minutes.

Sometimes a PIN can be missed due to server lag. This was the inspiration for the rewind feature. If a PIN misses, simply rewind a few times, then unpause and try again.